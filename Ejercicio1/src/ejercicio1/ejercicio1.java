package ejercicio1;
/**
 *
 * @author jaime
 */
import java.io.File;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class ejercicio1 {
    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
		Scanner sc = new Scanner(System.in);
		String fichero = "juegos.xml";
		
		try {
			DocumentBuilder builder = factory.newDocumentBuilder(); 
			Document document = builder.parse(new File(fichero)); 
			document.getDocumentElement().normalize();
					
			System.out.println("Elemento raíz: " + document.getDocumentElement() .getNodeName());
		
			NodeList jugadores = document.getElementsByTagName("juego");			
			for (int i = 0; i < jugadores.getLength(); i ++) { 
				Node jugador = jugadores.item(i); 
				
				if (jugador.getNodeType() == Node.ELEMENT_NODE) {
					Element elemento = (Element) jugador; 
					NodeList nodo= elemento.getElementsByTagName("nombre").item(0).getChildNodes(); 
					Node valornodo = (Node) nodo.item(0);
					
					System.out.println("-> Nombre: " + valornodo.getNodeValue()); 
				} 
			}

			System.out.println("-> Datos del nuevo juego:");
			String nombre, genero, plataforma, fechadelanzamiento;
			System.out.println("Inserta un nombre");
			nombre = sc.nextLine();
			System.out.println("Inserta un genero");
			genero = sc.nextLine();
			System.out.println("Inserta una plataforma");
			plataforma = sc.nextLine();
			System.out.println("Inserta una fecha de lanzamiento");
			fechadelanzamiento = sc.nextLine();
			
			Element raiz = document.createElement("juego"); 
			document.getDocumentElement().appendChild(raiz); 
			
			Element elem = document.createElement("nombre"); 
			Text text = document.createTextNode(nombre); 
			raiz.appendChild(elem);  
			elem.appendChild(text); 
			
			Element elem2 = document.createElement("genero"); 
			Text text2 = document.createTextNode(genero); 
			raiz.appendChild(elem2);
			elem2.appendChild(text2); 
			
			Element elem3 = document.createElement("plataforma"); 
			Text text3 = document.createTextNode(plataforma); 
			raiz.appendChild(elem3); 
			elem3.appendChild(text3); 
                        
			Element elem4 = document.createElement("fechadelanzamineto");
			Text text4 = document.createTextNode(fechadelanzamiento); 
			raiz.appendChild(elem4); 
			elem4.appendChild(text4); 
			
			Source source = new DOMSource(document); 
			Result result = new StreamResult(new java.io.File("juegosNuevos.xml")); 
                        
                        Transformer transformer = TransformerFactory.newInstance() .newTransformer(); 
			transformer.transform(source, result);
                        
			Source source2 = new DOMSource(document); 
                        
                        Transformer transformer2 = TransformerFactory.newInstance() .newTransformer(); 
			
			System.out.println("-> Inserta el nombre del juego que quieres borrar.");
			String borrar = sc.nextLine();
			
			NodeList borrarjuegos = document.getElementsByTagName("juego");
			for (int i = 0; i < borrarjuegos.getLength(); i ++) { 
				Node juego = borrarjuegos.item(i);  
				if (juego.getNodeType() == Node.ELEMENT_NODE) { 
					Element elemento = (Element) juego; 
					
					NodeList nodo= elemento.getElementsByTagName("nombre").item(0).getChildNodes(); 
					Node valornodo = (Node) nodo.item(0); 
					if(valornodo.getNodeValue().equals(borrar)) {
						juego.getParentNode().removeChild(juego);
					}
				}
			}
                        
			
			Result result2 = new StreamResult(new java.io.File("juegoBorrado.xml")); 			
			transformer2.transform(source2, result2);                       
			sc.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}       
    }   
}

