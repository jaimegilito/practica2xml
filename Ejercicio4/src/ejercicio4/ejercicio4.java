package ejercicio4;
/**
 *
 * @author jaime
 */
import java.util.ArrayList;
import java.util.List;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ejercicio4 {
    public static void main(String[] args) {     
        ArrayList<Cafe> cafes = new ArrayList<Cafe>();
        Cafe cafe1 = new Cafe("CafeIESCE" , (float) 5.3 , 43);
        Cafe cafe2 = new Cafe("CafeIESSanFer" , (float) 6.8 , 45);

        cafes.add(cafe1);
        cafes.add(cafe2);
        
        Proveedor proveedor1 = new Proveedor(150 , "Gilito" , "Avenida España" , "Madrid" , "España" , 12345 , true , cafes);      
        XStream xstream = new XStream(new DomDriver());
        
        xstream.alias("Proveedor" , Proveedor.class);
        xstream.aliasAttribute(Proveedor.class , "identificador" , "");
        xstream.aliasAttribute(Proveedor.class , "nombre" , "");
        xstream.aliasField("cif" , Proveedor.class , "identificador");
        xstream.aliasField("empresa" , Proveedor.class , "nombre");
        
        xstream.alias("Cafe" , Cafe.class);       
        String salida = xstream.toXML(proveedor1);      
        File fichero = new File("proveedor.xml");
      
        try {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(fichero))) {
                bw.write(salida);
                bw.flush();
            }
            try {            
                try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
                    String linea = "";                  
                    while (linea != null) {                      
                        linea = br.readLine();                       
                        System.out.print(linea + "\n");                      
                    }
                }
            } catch (IOException e) {              
                System.err.println("ERROR!");
            }
        } catch (IOException e) {           
            System.err.println("ERROR!");           
        }      
    }   
}

class Cafe {
	private String marca;
	private float precio;
	private int ventas;
        
        public Cafe(String marca , float precio , int ventas){         
            this.marca = marca;
            this.precio = precio;
            this.ventas = ventas;         
        }
        
	public String getMarca() {
		return marca;
	}

	public void setMarca(String nombre) {
		this.marca = nombre;
	}
	
	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getVentas() {
		return ventas;
	}

	public void setVentas(int ventas) {
		this.ventas = ventas;
	}

	 @Override
	    public String toString() {
	        return "Cafe{" + "marca=" + marca + "precio=" + precio+ "ventas=" + ventas + '}';
	    }

}

class Proveedor {
	private int identificador;
	private String nombre;
	private String calle;
	private String ciudad;
	private String pais;
	private int cp;
	private boolean esNacional;
	private List<Cafe> cafes;
		
	public Proveedor(int identificador , String nombre , String calle , String ciudad , String pais , int cp , boolean esNacional , List<Cafe> cafes) {
            super();
            this.identificador = identificador;
            this.nombre = nombre;
            this.calle = calle;
            this.ciudad = ciudad;
            this.pais = pais;
            this.cp = cp;
            this.esNacional = esNacional;
            this.cafes = cafes;
            
		cafes=new ArrayList<Cafe>();
	}	
	public void addCafe(Cafe cafe){
		cafes.add(cafe);		
	}
        
	public boolean isEsNacional() {
		return esNacional;
	}
        
	public void setEsNacional(boolean esNacional) {
		this.esNacional = esNacional;
	}
        
	public List<Cafe> getCafes() {
		return cafes;
	}
        
	public void setCafes(List<Cafe> cafes) {
		this.cafes = cafes;
	}
        
	public int getIdentificador() {
		return identificador;
	}
        
	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}
        
	public String getNombre() {
		return nombre;
	}
        
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
        
	public String getCalle() {
		return calle;
	}
        
	public void setCalle(String calle) {
		this.calle = calle;
	}
        
	public String getCiudad() {
		return ciudad;
	}
        
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
        
	public String getPais() {
		return pais;
	}
        
	public void setPais(String pais) {
		this.pais = pais;
	}
        
	public int getCp() {
		return cp;
	}
        
	public void setCp(int cp) {
		this.cp = cp;
	}
        
	@Override
	public String toString() {
		return "Proveedor [identificador=" + identificador + ", nombre="
				+ nombre + ", calle=" + calle + ", ciudad=" + ciudad
				+ ", pais=" + pais + ", cp=" + cp + ", esNacional="
				+ esNacional + ", cafes=" + cafes.toString() + "]";
	}      
}