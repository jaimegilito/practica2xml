package ejercicio3;
/**
 *
 * @author jaime
 */
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ejercicio3 {
    public static void main(String[] args) {
        Cafe cafe1 = new Cafe("CafeIESCE", (float) 5.3, 43, 57);
        Cafe cafe2 = new Cafe("CapuccinoSL", (float) 1.50, 19, 60);      
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("cafe", Cafe.class);
        String salida = xstream.toXML(cafe1) + "\n" + xstream.toXML(cafe2);       
        File fichero = new File("cafes.xml");
        
        try {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(fichero))) {
                bw.write(salida);
                bw.flush();
            }
            
            try {               
                try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
                    String linea = "";                  
                    while (linea != null) {                       
                        linea = br.readLine();
                        
                        System.out.print(linea + "\n");                      
                    }
                }
            } catch (IOException e) {             
                System.err.println("ERROR!");
            }
        } catch (IOException e) {          
            System.err.println("ERROR!");         
        }	
    } 
}

class Cafe {
	private String marca;
	private float precio;
	private int ventas;
	private int total;
        
        public Cafe(String marca , float precio , int ventas , int total){           
            this.marca = marca;
            this.precio = precio;
            this.ventas = ventas;
            this.total = total;          
        }
        
	public String getMarca() {
		return marca;
	}

	public void setMarca(String nombre) {
		this.marca = nombre;
	}

	
	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getVentas() {
		return ventas;
	}

	public void setVentas(int ventas) {
		this.ventas = ventas;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	 @Override
	    public String toString() {
	        return "Cafe{" + "marca=" + marca + "precio=" + precio+ "ventas=" + ventas + "total=" + total +'}';
	    }

}

