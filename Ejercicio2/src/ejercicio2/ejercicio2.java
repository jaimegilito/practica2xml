package ejercicio2;
/**
 *
 * @author jaime
 */
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class ejercicio2 {
    public static void main(String[] args) {       
        DocumentBuilderFactory factoria = DocumentBuilderFactory.newInstance();
        //LEER
        try { 
            DocumentBuilder builder = factoria.newDocumentBuilder(); 
            Document documento = builder.parse(new File("gamers.xml"));              
            documento.getDocumentElement().normalize(); 
            NodeList ListaGamers = documento.getElementsByTagName("gamer"); 				
            for (int i = 0; i < ListaGamers.getLength(); i ++) {                     
                Node gamer = ListaGamers.item(i);  
                if (gamer.getNodeType() == Node.ELEMENT_NODE) {                      
                    Element elemento = (Element) gamer;
                        
                    System.out.println("Apodo: " + getNodo("apodo", elemento)); 
                    System.out.println("Nombre: " + getNodo("nombre", elemento)); 
                    System.out.println("Videojuego: " + getNodo("videojuego", elemento)); 
                    System.out.println("Edad: " + getNodo("edad", elemento)); 
                    System.out.println("Origen: " + getNodo("origen", elemento));                       
                }               
                System.out.println("");                
		}
            } catch (IOException | ParserConfigurationException | SAXException e) {               
                System.err.println("ERROR!");               
            }            
    }
    
	private static String getNodo(String etiqueta, Element elem) {            
		NodeList nodo = elem.getElementsByTagName(etiqueta).item(0).getChildNodes(); 
		Node valornodo = (Node) nodo.item(0); 
		return valornodo.getNodeValue();      
	}    
}
