package ejercicio5;
/**
 *
 * @author jaime
 */
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ejercicio5 {

    public static void main(String[] args) {
      
        System.out.println("Creando alumno 1...");
        Domicilio domicilio1 = new Domicilio();
        Alumno alumno1 = new Alumno(domicilio1);

        System.out.println("Creando alumno 2...");
        Domicilio domicilio2 = new Domicilio();
        Alumno alumno2 = new Alumno(domicilio2);
 
        Aula aula1 = new Aula(1);
      
        aula1.add(alumno1);
        aula1.add(alumno2);
        
     
        
        XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
        xstream.alias("Aula" , Aula.class);
        xstream.alias("Alumno" , Alumno.class);
        
        xstream.alias("Domicilio", Domicilio.class);
        
        String salida = xstream.toXML(aula1);
   
        File fichero = new File("aula.xml");    
        try {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(fichero))) {
                bw.write(salida);
                bw.flush();
            }
            try {             
                try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
                    String linea = "";                   
                    while (linea != null) {                       
                        linea = br.readLine();
                      
                        System.out.print(linea + "\n");                       
                    }
                }
            } catch (IOException e) {
                
                System.err.println("ERROR!");
            }
        } catch (IOException e) {
            
            System.err.println("ERROR!");          
        }
    }   
}

class Alumno {

	String nombre;
	String apellidos;
	int anoNacimiento;
        Domicilio dom;
	
	public Alumno(Domicilio domicilio) {         
            Scanner sc = new Scanner(System.in);
            
            System.out.println("Introduce el nombre del alumno: ");
            String nombre = sc.nextLine();
            this.nombre = nombre;
            
            System.out.println("Introduce los apellidos del alumno: ");
            String apellidos = sc.nextLine();
            this.apellidos = apellidos;
            
            System.out.println("Introduce el año de nacimiento del alumno: ");
            int anoNacimiento = sc.nextInt();
            this.anoNacimiento = anoNacimiento;
            
            this.dom = domicilio;
	}
        
	public String getNombre() {
		return nombre;
	}
        
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
        
	public String getApellidos() {
		return apellidos;
	}
        
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
        
	public int getAnoNacimiento() {
		return anoNacimiento;
	}
        
	public void setAnoNacimiento(int anoNacimiento) {
		this.anoNacimiento = anoNacimiento;
	}

	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + ", apellidos=" + apellidos
				+ ", anoNacimiento=" + anoNacimiento + ", domicilio=" + dom + "]";
	}
		
}

class Aula {
    
    private List<Alumno> alumnos;
    private Date fechaCreacion;
 
    public Aula(int tamano){
        alumnos=new ArrayList<Alumno>(tamano);
    }

    public void add(Alumno alumno){        
            alumnos.add(alumno);            
    }

    public boolean eliminar(Alumno alumno){
    	return alumnos.remove(alumno);    
    }
         
    public List<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public String toString() {
		DateFormat formatter = DateFormat.getDateInstance(DateFormat.FULL,
                new Locale("es"));
		return "Aula [alumnos=" + alumnos.toString() + ", fechaCreacion=" + formatter.format(fechaCreacion)
				+ "]";
	}
}

class Domicilio{
    
    String calle;
    int numero;
    
    public Domicilio(){       
        Scanner sc = new Scanner(System.in); 
        
        System.out.println("Introduce el nombre de la calle del alumno: ");
 
        String calle = sc.nextLine();
        this.calle = calle;  
        
        System.out.println("Introduce el numero de la vivienda del alumno: ");
 
        int numero = sc.nextInt();
        this.numero = numero;    
    }
    
    public String getCalle() {
		return calle;
	}
    
	public void setCalle(String calle) {
		this.calle = calle;
	}
        
	public int getNumero() {
		return numero;
	}
        
	public void setAnoNacimiento(int numero) {
		this.numero = numero;
	}
        
        @Override
	public String toString() {
		return "Domicilio [calle=" + calle + ", numero=" + numero + "]";
	} 
}


